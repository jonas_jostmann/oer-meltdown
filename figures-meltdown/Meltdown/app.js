
Ext.define('Shopware.apps.LoremQuestions', {
    extend: 'Enlight.app.SubApplication',

    name:'Shopware.apps.LoremQuestions',

    loadPath: '{url action=load}',
    bulkLoad: true,

    controllers: [ 'Main' ],

    views: [
        'list.Window',
        'list.List',

        'detail.Container',
        'detail.Window',

        'answer.Listing',
    ],

    models: [ 'Question', 'Answer' ],
    stores: [ 'Question' ],

    launch: function() {
        return this.getController('Main').mainWindow;
    }
});